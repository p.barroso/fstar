module Pure_functional


type value =
  | VBool : bool -> value
  | VNat : int -> value

type bin_op =
  | PlusOp
  | MinusOp
  | LeOp
  | LtOp
  | EqOp

type expr =
  | EVar : string -> expr
  | EVal : value -> expr
  | ELet : string -> expr -> expr -> expr
  | EOp : bin_op -> expr -> expr -> expr 
  | EIf : expr -> expr -> expr -> expr


let eval_bin_op (op: bin_op) (v1 v2 : value) : option value
= match op, v1, v2 with
  | PlusOp, VNat n1, VNat n2 -> Some (VNat (n1 + n2))
  | MinusOp, VNat n1, VNat n2 -> Some (VNat (n1 - n2))
  | LeOp, VNat n1, VNat n2 -> Some (VBool (n1 <= n2))
  | LtOp, VNat n1, VNat n2 -> Some (VBool (n1 < n2))
  | EqOp, VNat n1, VNat n2 -> Some (VBool (n1 = n2))
  | EqOp, VBool n1, VBool n2 -> Some (VBool (n1 = n2))
  | _, _, _ -> None


let rec size (e:expr) : e':int { e' > 0 } 
= match e with
  | EVal _ -> 1
  | EVar _ -> 1
  | ELet y e1 e2 -> 1 + size e1 + size e2
  | EIf e1 e2 e3 -> 1 + size e1 + size e2 + size e3
  | EOp op e1 e2 -> 1 + size e1 + size e2

let size_positive (e:expr)
  : Lemma (ensures size e > 0)
= ()


let rec  subst (x : string) (w : value) (e : expr) : e':expr { size e == size e' }
= match e with
  | EVal v -> EVal v
  | EVar y -> if x = y then EVal w else EVar y
  | ELet y e1 e2 ->
      if x = y
      then ELet y (subst x w e1) e2
      else ELet y (subst x w e1) (subst x w e2)
  | EOp op e1 e2 -> EOp op (subst x w e1) (subst x w e2)
  | EIf e1 e2 e3 -> EIf (subst x w e1) (subst x w e2) (subst x w e3)

let subst_preserves_size (e: expr) (y: string) (v: value)
  : Lemma  (ensures size e = size (subst y v e))
= ()

type big_step : expr -> value -> Type =
  | Val_big_step : v: value -> big_step (EVal v) v
  | Let_big_step : y : string -> e1 : expr -> e2 : expr -> v1 : value -> v2 : value ->
                   big_step e1 v1 ->
                   big_step (subst y v1 e2) v2 ->
                   big_step (ELet y e1 e2) v2
  | If_big_step_true : e1 : expr -> e2 : expr -> e3 : expr -> v : value ->
                       big_step e1 (VBool true) ->
                       big_step e2 v ->
                       big_step (EIf e1 e2 e3) v
  | If_big_step_false : e1 : expr -> e2 : expr -> e3 : expr -> v : value ->
                      big_step e1 (VBool false) ->
                      big_step e3 v ->
                      big_step (EIf e1 e2 e3) v
  | Op_big_step : e1 : expr -> e2 : expr -> op : bin_op -> v1 : value -> v2 : value -> v : value ->
                       big_step e1 v1 ->
                       big_step e2 v2 ->
                       squash (eval_bin_op op v1 v2 == Some v) ->
                       big_step (EOp op e1 e2) v
(*
let big_step_example1 = assert (big_step example1 (VBool true))
*)


let big_step_eval ()
  : Lemma (forall v. big_step (EVal v) v)
= introduce
  forall v. big_step (EVal v) v
  with FStar.Squash.return_squash (Val_big_step v)

let big_step_eval_1 (v: value)
  : Lemma (big_step (EVal v) v)
= FStar.Squash.return_squash (Val_big_step v)

(*
let big_step_if_true_true_true ()
  : Lemma (forall v. big_step (EIf (EVal v) (EVal v) (EVal v) ) v)
= introduce
  forall v. big_step (EIf (EVal v) (EVal v) (EVal v)) v
  with FStar.Squash.return_squash (If_big_step_true (EVal v) (EVal v) (EVal v) v)*)


let q = big_step (EIf (EVal (VBool true)) (EVal (VBool true)) (EVal (VBool true))) (VBool true)

//The annotations on p and p1 are not necessary, writing only as explanations
let big_step_simple_if () : Lemma q =
  //We first build a constructive proof of q
  let p : big_step (EVal (VBool true)) (VBool true) = Val_big_step (VBool true) in
  let p1 : q = If_big_step_true _ _ (EVal (VBool true)) _ p p in
  //And then squash it to prove it as a lemma
  FStar.Squash.return_squash p1


let q1 = big_step (EIf (EVal (VBool false)) (EVal (VBool false)) (EVal (VBool false))) (VBool false)

//The annotations on p and p1 are not necessary, writing only as explanations
let big_step_simple_if_2 () : Lemma q1 =
  //We first build a constructive proof of q
  let p : big_step (EVal (VBool false)) (VBool false) = Val_big_step (VBool false) in
  let p1 : q1 = If_big_step_false _ _ (EVal (VBool false)) _ p p in
  //And then squash it to prove it as a lemma
  FStar.Squash.return_squash p1

(*
let basic1 () : Lemma (eval_bin_op PlusOp (VNat 10) (VNat 20) == Some (VNat (10 + 20))) = ()*)

let example1_aux () : Lemma (eval_bin_op PlusOp (VNat 10) (VNat 20) == Some (VNat 30)) = ()

let example1 = big_step (EOp EqOp (EOp PlusOp (EVal (VNat 10)) (EVal (VNat 20))) (EVal (VNat 30))) (VBool true)
let example2 = big_step (EOp PlusOp (EVal (VNat 10)) (EVal (VNat 20))) (VNat 30)

let big_step_example2 () : Lemma example2 =
  let p : big_step (EVal (VNat 10)) (VNat 10) = Val_big_step (VNat 10) in
  let p1 : big_step (EVal (VNat 20)) (VNat 20) = Val_big_step (VNat 20) in
  let p2 : squash (eval_bin_op PlusOp (VNat 10) (VNat 20) == Some (VNat 30)) = () in
  let p3 : example2 = Op_big_step (EVal (VNat 10)) (EVal (VNat 20)) PlusOp (VNat 10) (VNat 20) (VNat 30) p p1 p2 in
  FStar.Squash.return_squash p3

let big_step_example1 () : Lemma example1 =
  let p : big_step (EVal (VNat 10)) (VNat 10) = Val_big_step (VNat 10) in
  let p1 : big_step (EVal (VNat 20)) (VNat 20) = Val_big_step (VNat 20) in
  let p2 : squash (eval_bin_op PlusOp (VNat 10) (VNat 20) == Some (VNat 30)) = () in
  let p3 : example2 = Op_big_step (EVal (VNat 10)) (EVal (VNat 20)) PlusOp (VNat 10) (VNat 20) (VNat 30) p p1 p2 in
  let p4 : big_step (EVal (VNat 30)) (VNat 30) = Val_big_step (VNat 30) in
  let p5 : squash (eval_bin_op EqOp (VNat 30) (VNat 30) == Some (VBool true)) = () in
  let p6 : example1 = Op_big_step (EOp PlusOp (EVal (VNat 10)) (EVal (VNat 20))) (EVal (VNat 30)) EqOp (VNat 30) (VNat 30) (VBool true) p3 p4 p5 in
  FStar.Squash.return_squash p6

let big_step_determinism (e: expr) (v1 v2: value) 
  : Lemma (requires big_step e v1 /\ big_step e v2)
          (ensures v1 == v2)
= match e with
  | EVal v -> Val_big_step v
  | EVar y -> admit()
  | ELet y e1 e2 -> admit()
  | EOp op e1 e2 -> admit()
  | EIf e1 e2 e3 -> admit()

(* Tentar com a linaguagem do Pierce *)
                     
  

