module Pierce_Arith

  type term =
    | TmTrue
    | TmFalse
    | TmIf : term -> term -> term -> term
    | TmZero
    | TmSucc : term -> term
    | TmPred : term -> term
    | TmIsZero : term -> term


  let rec isnumericval t
  = match t with
    | TmZero -> true
    | TmSucc t1 -> isnumericval t1
    | _ -> false


  let rec isval t
  = match t with
    | TmTrue -> true
    | TmFalse -> true
    | t -> isnumericval t
    | _ -> false


  type small_step : term -> term -> Type =
    | TmIfTrue_ss : t2 : term -> t3 : term ->
                 small_step (TmIf TmTrue t2 t3) t2
    | TmIfFalse_ss : t2 : term -> t3 : term ->
                 small_step (TmIf TmFalse t2 t3) t3
    | TmIf_ss : t1 : term -> t2 : term -> t3 : term -> t1' : term ->
                 small_step t1 t1' ->
                 small_step (TmIf t1 t2 t3) (TmIf t1' t2 t3)
    | TmSucc_ss : t1 : term -> t1' : term ->
                 small_step t1 t1' ->
                 small_step (TmSucc t1) (TmSucc t1')
    | TmPredZero_ss : 
                 small_step (TmPred (TmZero)) TmZero
    | TmPredSucc_ss : nv1 : term ->
                 isnumericval nv1 == true ->
                 small_step (TmPred (TmSucc nv1)) nv1
    | TmPred_ss : t1 : term -> t1' : term ->
                 small_step t1 t1' ->
                 small_step (TmPred t1) (TmPred t1')
    | TmIsZeroZero_ss : 
                 small_step TmZero TmTrue
    | TmIsZeroSucc_ss : nv1 : term ->
                 isnumericval nv1 == true ->
                 small_step (TmIsZero (TmSucc nv1)) TmFalse
    | TmIsZero_ss : t1 : term -> t1' : term ->
                 small_step t1 t1' ->
                 small_step (TmIsZero t1) (TmIsZero t1')
    
