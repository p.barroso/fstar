 
module Nielsons_Arith

  type a_op =
    | PLUS
    | MULT
    | MINUS


  type a_exp =
    | Int_e : int -> a_exp
    | Minus_e : a_exp -> a_exp
    | Op_e : a_exp -> a_op -> a_exp -> a_exp


  let rec a_exp_eval (aexp : a_exp) : int
  = match aexp with
    | Int_e n -> n
    | Minus_e a1 -> 0 - (a_exp_eval a1)
    | Op_e a1 PLUS a2 -> (a_exp_eval a1) + (a_exp_eval a2)
    | Op_e a1 MULT a2 -> op_Multiply (a_exp_eval a1) (a_exp_eval a2)
    | Op_e a1 MINUS a2 -> (a_exp_eval a1) - (a_exp_eval a2)

  type big_step : a_exp -> int -> Type =
  | Int_e_big_step : n : int -> big_step (Int_e n) n
  | Minus_e_big_step : x : a_exp -> v : int -> 
                       big_step x v ->
                       big_step (Minus_e x) (0 - v)
  | Plus_big_step : a1 : a_exp -> a2 : a_exp -> v1 : int -> v2 : int ->
                       big_step a1 v1 ->
                       big_step a2 v2 ->
                       big_step (Op_e a1 PLUS a2) (v1 + v2)
  | Mult_big_step : a1 : a_exp -> a2 : a_exp -> v1 : int -> v2 : int ->
                       big_step a1 v1 ->
                       big_step a2 v2 ->
                       big_step (Op_e a1 MULT a2) (op_Multiply v1 v2)
  | Minus_big_step : a1 : a_exp -> a2 : a_exp -> v1 : int -> v2 : int ->
                       big_step a1 v1 ->
                       big_step a2 v2 ->
                       big_step (Op_e a1 MINUS a2) (v1 - v2)

  let arith_example = big_step (Op_e (Int_e 2) PLUS (Int_e 2)) 4

  let add_2_2 () : Lemma arith_example
  = let p1 : big_step (Int_e 2) 2 = Int_e_big_step 2 in
    let p2 : arith_example = Plus_big_step (Int_e 2) (Int_e 2) 2 2 p1 p1 in
    FStar.Squash.return_squash p2


  let add_2 (v : int) : Lemma (big_step (Op_e (Int_e v) PLUS (Int_e 2)) (v + 2))
  = let p1 : big_step (Int_e v) v = Int_e_big_step v in
    let p2 : big_step (Int_e 2) 2 = Int_e_big_step 2 in
    let p3 : big_step (Op_e (Int_e v) PLUS (Int_e 2)) (v + 2) = Plus_big_step (Int_e v) (Int_e 2) v 2 p1 p2 in
    FStar.Squash.return_squash p3


  let add_generic (v1 v2 : int) : Lemma (big_step (Op_e (Int_e v1) PLUS (Int_e v2)) (v1 + v2))
  = let p1 : big_step (Int_e v1) v1 = Int_e_big_step v1 in
    let p2 : big_step (Int_e v2) v2 = Int_e_big_step v2 in
    let p3 : big_step (Op_e (Int_e v1) PLUS (Int_e v2)) (v1 + v2) = Plus_big_step (Int_e v1) (Int_e v2) v1 v2 p1 p2 in
    FStar.Squash.return_squash p3

  let example1 () : Lemma (big_step (Op_e (Op_e (Int_e 2) PLUS (Int_e 2)) MULT (Op_e (Int_e 4) MINUS (Int_e 2))) 8)
  = let p1 : big_step (Int_e 2) 2 = Int_e_big_step 2 in
    let p2 : big_step (Int_e 4) 4 = Int_e_big_step 4 in
    let p3 : big_step (Op_e (Int_e 2) PLUS (Int_e 2)) 4 = Plus_big_step (Int_e 2) (Int_e 2) 2 2 p1 p1 in
    let p4 : big_step (Op_e (Int_e 4) MINUS (Int_e 2)) 2 = Minus_big_step (Int_e 4) (Int_e 2) 4 2 p2 p1 in
    let p5 : big_step (Op_e (Op_e (Int_e 2) PLUS (Int_e 2)) MULT (Op_e (Int_e 4) MINUS (Int_e 2))) 8 = Mult_big_step (Op_e (Int_e 2) PLUS (Int_e 2)) (Op_e (Int_e 4) MINUS (Int_e 2)) 4 2 p3 p4 in
    FStar.Squash.return_squash p5
